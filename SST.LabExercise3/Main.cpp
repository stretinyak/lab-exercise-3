#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

void Madlib(string answer[]);

int main()
{
	const int ENTRIES = 10;
	string questions[ENTRIES];

	questions[0] = "Enter an adjective <describing word>: ";
	questions[1] = "Enter a sport: ";
	questions[2] = "Enter a city name: ";
	questions[3] = "Enter a person: ";
	questions[4] = "Enter an action verb <past tense>: ";
	questions[5] = "Enter a vehicle: ";
	questions[6] = "Enter a palce: ";
	questions[7] = "Enter a noun <thing, plural>: ";
	questions[8] = "Enter an adjective <describing word>: ";
	questions[9] = "Enter a food <plural>: ";

	string answer[ENTRIES];

	for (int i = 0; i < ENTRIES; i++)
	{
		cout << questions[i];
		cin >> answer[i];
	}

	Madlib(answer);

	char input = 'y';
	cout << "\n\n" << "Would you like to save a text file (y/n)? ";
	cin >> input;

	while (input == 'y')
	{
		string path = "C:\\Users\\samtr\\Desktop\\test.txt";

		ofstream ofs(path);
		ofs << "One day my " << answer[0] << " friend and I decided to go to the " << answer[1] << " in " << answer[2] << "." << "\n" << "We really wanted to see " << answer[3] << " play." << "\n" << "So we " << answer[4] << " in the " << answer[5] << " and headed down to the " << answer[6] << " and bought some " << answer[7] << ". " << "\n" << "We watched the game and it was " << answer[8] << ". " << "\n" << "We ate some " << answer[9] << ".";
		ofs.close();
	}

	_getch();
	return 0;
}

void Madlib(string answer[])
{
	cout << "\n" << "One day my " << answer[0] << " friend and I decided to go to the " << answer[1] << " in " << answer[2] << "." << "\n" << "We really wanted to see " << answer[3] << " play." << "\n" << "So we " << answer[4] << " in the " << answer[5] << " and headed down to the " << answer[6] << " and bought some " << answer[7] << ". " << "\n" << "We watched the game and it was " << answer[8] << ". " << "\n" << "We ate some " << answer[9] << ".";
}